﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace VkApiClient.Models
{
    public class VkPost
    {
        /// <summary>
        /// Идентификатор поста в вк.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonConverter(typeof(UnixDateTimeConverter))]
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("from_id")]
        public int AuthorId { get; set; }

    }
}