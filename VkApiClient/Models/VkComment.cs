﻿using Newtonsoft.Json;

namespace VkApiClient.Models
{
    public class VkComment
    {
        /// <summary>
        /// Идентификатор коммента в вк.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("from_id")]
        public int AuthorId { get; set; }        
    }
}