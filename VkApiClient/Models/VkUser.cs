﻿using Newtonsoft.Json;

namespace VkApiClient.Models
{
    public class VkUser
    {
        /// <summary>
        /// Идентификатор пользователя в вк.
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }
    }
}