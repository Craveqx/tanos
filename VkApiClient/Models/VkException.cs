﻿using System;

namespace VkApiClient.Models
{
    /// <summary>
    /// Исключение генерируемое при ошибке запроса к vk api.
    /// </summary>
    public class VkException: Exception
    {
        private readonly VkError _error;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="error"><see cref="VkError"/>.</param>
        public VkException(VkError error):base(error.Message)
        {
            _error = error ?? throw new ArgumentNullException(nameof(error));
            
        }

        /// <summary>
        /// Код ошибки.
        /// </summary>
        public int Code => _error.Code;
    }
}