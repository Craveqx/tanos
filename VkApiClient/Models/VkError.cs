﻿using Newtonsoft.Json;

namespace VkApiClient.Models
{
    /// <summary>
    /// Ошибка вк.
    /// </summary>
    public class VkError
    {
        /// <summary>
        /// Код ошибки.
        /// </summary>
        [JsonProperty("error_code")]
        public int Code { get; set; }

        /// <summary>
        /// Сообщение.
        /// </summary>
        [JsonProperty("error_msg")]
        public string Message { get; set; }
    }
}