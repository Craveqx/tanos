﻿using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using VkApiClient.Models;

namespace VkApiClient.Providers
{
    public interface IVkAccessTokenProvider
    {
        /// <summary>
        /// Возвращает токен.
        /// </summary>
        /// <returns>Объект токена.</returns>
        Task<Result<VkAccessToken>> GetToken();
    }
}