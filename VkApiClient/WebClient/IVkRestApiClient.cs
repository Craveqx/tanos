﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VkApiClient.WebClient
{
    /// <summary>
    /// Клиент для запросов к ВК по rest API.
    /// </summary>
    public interface IVkRestApiClient
    {
        /// <summary>
        /// Отправить запрос.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого объекта.</typeparam>
        /// <param name="requestParams">Параметры запроса.</param>
        /// <param name="methodName">Имя вызываемого метода api.</param>
        /// <returns>Результат запроса.</returns>
        Task<T> SendRequest<T>(List<KeyValuePair<string, string>> requestParams, string methodName);
    }
}