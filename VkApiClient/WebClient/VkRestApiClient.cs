﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VkApiClient.Models;
using VkApiClient.Providers;

namespace VkApiClient.WebClient
{
    /// <summary>
    /// Клиент для запросов к VK API.
    /// </summary>
    public class VkRestApiClient : IVkRestApiClient
    {
        private readonly IVkAccessTokenProvider _tokenProvider;
        private readonly string _vkApiVersion;
        private const string VkbaseApiUrl = "https://api.vk.com/method";

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="tokenProvider">Провайдер токена доступа.</param>
        /// <param name="vkApiVersion">Версия vk api.</param>
        public VkRestApiClient(IVkAccessTokenProvider tokenProvider, string vkApiVersion = "5.75")
        {
            _tokenProvider = tokenProvider;
            _vkApiVersion = vkApiVersion;
        }

        /// <summary>
        /// Отправить запрос.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого объекта.</typeparam>
        /// <param name="requestParams">Параметры запроса.</param>
        /// <param name="methodName">Имя вызываемого метода api.</param>
        /// <returns>Результат запроса.</returns>
        public  async Task<T> SendRequest<T>(List<KeyValuePair<string, string>> requestParams, string methodName)
        {
            var tokenGetResult = await _tokenProvider.GetToken();
            if (!tokenGetResult.IsSuccess)
            {
                throw new Exception(tokenGetResult.Error);
            }

            var paramList = CreateParams(tokenGetResult.Value.Token, requestParams);
            var httpClient = new HttpClient();
            var content = new FormUrlEncodedContent(paramList);
            var result = await httpClient.PostAsync($"{VkbaseApiUrl}/{methodName}", content);
            result.EnsureSuccessStatusCode();
            var response = await result.Content.ReadAsStringAsync();
            return ParseResponse<T>(response);
        }

        private  T ParseResponse<T>(string response)
        {
            if (!(JsonConvert.DeserializeObject(response) is JObject jsonOb))
            {
                throw new Exception("Response empty");
            }

            if (jsonOb.ContainsKey("error"))
            {
                var error = jsonOb["error"].ToObject<VkError>();
                throw new VkException(error);
            }

            return jsonOb["response"].ToObject<T>();
        }

        private List<KeyValuePair<string, string>> CreateParams(string token, List<KeyValuePair<string, string>> requestParams)
        {
            var result = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("v", _vkApiVersion)
            };
            if (!string.IsNullOrWhiteSpace(token))
            {
                result.Add(new KeyValuePair<string, string>("access_token", token));
            }
            result.AddRange(requestParams);
            return result;
        }
    }
}