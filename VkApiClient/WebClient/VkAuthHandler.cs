﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using CSharpFunctionalExtensions;
using HtmlAgilityPack;
using VkApiClient.Models;

namespace VkApiClient.WebClient
{
    public class VkAuthHandler
    {
        private const string AuthRequestUri =
            "https://oauth.vk.com/authorize?client_id=6033143revoke=0&display=mobile&redirect_uri=https://oauth.vk.com/blank.html&response_type=token";

        private const string UserAgent = "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0";
        private const string AccessTokenParam = "access_token";
        private const string ExpiresInParam = "expires_in";

        public static async Task<Result<VkAccessToken>> Login(NetworkCredential clientCredential)
        {
            using (var httpClient = CreateHttpClientWithCookies())
            {
                var authRequest = await httpClient.GetAsync(AuthRequestUri);
                var htmlContent = await authRequest.Content.ReadAsStreamAsync();

                var doc = new HtmlDocument();
                doc.Load(htmlContent);
                var formDataDictionary = ParseDocumentAndGetPostDictionary(doc);
                AddUserCredentials(formDataDictionary, clientCredential);
                var postUrl = ParseDocAndGetFormUrl(doc);
                var authFormPostResponse =
                    await httpClient.PostAsync(postUrl, new FormUrlEncodedContent(formDataDictionary));
                if (authFormPostResponse.StatusCode == HttpStatusCode.Found)
                {
                    var grantAccessResponse = await httpClient.GetAsync(authFormPostResponse.Headers.Location);
                    if (grantAccessResponse.StatusCode == HttpStatusCode.Found)
                    {
                        var grantAccessResult = await httpClient.GetAsync(grantAccessResponse.Headers.Location);
                        if (grantAccessResult.Headers.Location.ToString().Contains(AccessTokenParam))
                            return ParseAccessTokenFromUrl(grantAccessResult.Headers.Location);
                    }

                    return Result.Fail<VkAccessToken>("Grant access response StatusCode not 302");
                }

                return Result.Fail<VkAccessToken>("Auth post response StatusCode not 302");
            }
        }

        private static Result<VkAccessToken> ParseAccessTokenFromUrl(Uri redirectLocation)
        {
            var paramDictonary = HttpUtility.ParseQueryString(redirectLocation.Fragment.Substring(1));
            if (!paramDictonary.AllKeys.Contains(ExpiresInParam))
            {
                return Result.Fail<VkAccessToken>("ExpiresInParam not found in response url");
            }
            if (!paramDictonary.AllKeys.Contains(AccessTokenParam))
            {
                return Result.Fail<VkAccessToken>("Access token not found in response url");
            }

            if (!int.TryParse(paramDictonary[ExpiresInParam], out int expiredIn))
            {
                return Result.Fail<VkAccessToken>("ExpiresInParam not int type");
            }
            var token = new VkAccessToken(paramDictonary[AccessTokenParam], expiredIn);
            return Result.Ok(token);
        }

        private static void AddUserCredentials(Dictionary<string, string> dataDictionary,
            NetworkCredential clientCredential)
        {
            dataDictionary.Add("email", clientCredential.UserName);
            dataDictionary.Add("pass", clientCredential.Password);
        }

        private static string ParseDocAndGetFormUrl(HtmlDocument doc)
        {
            return doc.DocumentNode
                .SelectSingleNode("//form[@method='post']")
                .GetAttributeValue("action", "");
        }

        private static Dictionary<string, string> ParseDocumentAndGetPostDictionary(HtmlDocument doc)
        {
            return doc.DocumentNode
                .SelectNodes("//input[@type='hidden']")
                .ToDictionary(x => x.GetAttributeValue("name", ""), x => x.GetAttributeValue("value", ""));
        }

        private static HttpClient CreateHttpClientWithCookies()
        {
            var cookieContainer = new CookieContainer();
            var handler = new HttpClientHandler
            {
                CookieContainer = cookieContainer,
                AllowAutoRedirect = false
            };
            var httpClient = new HttpClient(handler);
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("VkUser-Agent", UserAgent);
            return httpClient;
        }
    }
}