﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VkApiClient.Models;
using VkApiClient.Providers;

namespace VkApiClient.WebClient
{
    
    /// <summary>
    /// Веб клиент для запросов к api vk.
    /// </summary>
    public class VkWebClient : IVkWebClient
    {
        private readonly IVkRestApiClient _restApiClient;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="vkRestApiClient">Клиент для запроса в вк.</param>
        public VkWebClient(IVkRestApiClient vkRestApiClient)
        {
            _restApiClient = vkRestApiClient ?? throw new ArgumentNullException(nameof(vkRestApiClient));
        }

        public async Task<VkUser> GetUserById(int id)
        {
            var requestParams = new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("user_ids", id.ToString())
                };
            var result = await _restApiClient.SendRequest<List<VkUser>>(requestParams,  "users.get");
            return result.FirstOrDefault();
        }

        public async Task<VkPost> GetPostById(string id)
        {
            var requestParams = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("posts", id)
            };
            var result = await _restApiClient.SendRequest<List<VkPost>>(requestParams, "wall.getById");
            return result.FirstOrDefault();
        }
    }
}