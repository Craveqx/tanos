﻿using System.Threading.Tasks;
using VkApiClient.Models;

namespace VkApiClient.WebClient
{
    public interface IVkWebClient
    {
        Task<VkUser> GetUserById(int id);
        Task<VkPost> GetPostById(string id);
    }
}