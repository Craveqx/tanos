using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Tanos.Models;
using Xunit;

namespace UnitTests
{
    public class VkRawEventDesirializeTest
    {
        [Fact]
        public void Deserialize_JsonIsValid_PropertyEqualsToExpected()
        {
            var json =
                "{\r\n  " +
                "\"type\":\"wall_reply_new\",\r\n  " +
                "\"object\": {\r\n    \"id\": 123,\r\n    \"from_id\": 123151,\r\n    \"date\": 1432436436,\r\n    \"text\": \"Hello bitches\"\r\n  },\r\n  " +
                "\"group_id\" :  124123 \r\n}";

            var rawEvent = JsonConvert.DeserializeObject<VkRawEvent>(json);

            var jobject = rawEvent.JsonObject as JObject;
            jobject.ToObject<WallComment>();
            Assert.Equal("wall_reply_new", rawEvent.Type);
           // Assert.Equal(123, rawEvent.Object);
        }
    }
}