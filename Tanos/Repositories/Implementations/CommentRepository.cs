﻿using Tanos.DatabaseContext;
using Tanos.Models.Dbo;
using Tanos.Repositories.Interfaces;

namespace Tanos.Repositories.Implementations
{
    public class CommentRepository : ICommentRepository
    {
        public void Add(Comment comment)
        {
            using (var context = new UserActivityContext())
            {
                context.Comments.Add(comment);
                context.SaveChanges();
            }
        }
    }
}