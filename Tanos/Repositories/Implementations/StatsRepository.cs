﻿using Tanos.Models.Dbo;
using Tanos.Repositories.Interfaces;
using StatsContext = Tanos.DatabaseContext.StatsContext;

namespace Tanos.Repositories.Implementations
{
    public class StatsRepository : IStatsRepository
    {
        public void AddStat(AppStat appStat)
        {
            using (var context = new StatsContext())
            {
                context.AppStat.Add(appStat);
                context.SaveChanges();
            }
        }

    }
}