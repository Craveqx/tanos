﻿using System;
using Tanos.DatabaseContext;
using Tanos.Models.Dbo;
using Tanos.Repositories.Interfaces;

namespace Tanos.Repositories.Implementations
{
    public class UserActivityRepository : IUserActivityRepository
    {
        public void Add(UserActivity userActivity)
        {
            using (var context = new UserActivityContext())
            {
                context.Add(userActivity);
                context.SaveChanges();
            }
        }
    }
}