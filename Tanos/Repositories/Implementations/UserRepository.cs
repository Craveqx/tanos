﻿using System.Linq;
using Tanos.DatabaseContext;
using Tanos.Models.Dbo;
using Tanos.Repositories.Interfaces;

namespace Tanos.Repositories.Implementations
{
public    class UserRepository : IUserRepository
    {
        public User GetUserByVkId(int id)
        {
            using (var context = new UserActivityContext())
            {
                return  context.Users.FirstOrDefault(x=>x.Id==id);
            }
        }

        public void Add(User user)
        {
            using (var context = new UserActivityContext())
            {
                context.Users.Add(user);
                context.SaveChanges();
            }
        }
    }
}