﻿using System;
using System.Linq;
using Tanos.DatabaseContext;
using Tanos.Models.Dbo;
using Tanos.Repositories.Interfaces;

namespace Tanos.Repositories.Implementations
{
    public class PostRepository : IPostRepository
    {
        public void Add(Post post)
        {
            using (var context = new UserActivityContext())
            {
                context.Posts.Add(post);
                context.SaveChanges();
            }
        }

        public Post Get(int id)
        {
            using (var context = new UserActivityContext())
            {
                return  context.Posts.FirstOrDefault(x=>x.Id==id);
            }
        }

        public Post GetByOwnerAndId(int postId, int wallOwnerId)
        {
            using (var context = new UserActivityContext())
            {
                return context.Posts.FirstOrDefault(x => x.Id == postId && x.AuthorId == wallOwnerId);
            }
        }
    }
}