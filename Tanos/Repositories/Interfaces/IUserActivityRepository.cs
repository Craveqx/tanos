﻿using Tanos.Models.Dbo;

namespace Tanos.Repositories.Interfaces
{
    public interface IUserActivityRepository
    {
        void Add(UserActivity userActivity);
    }
}