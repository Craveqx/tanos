﻿using Tanos.Models.Dbo;

namespace Tanos.Repositories.Interfaces
{
    public interface IUserRepository
    {
        User GetUserByVkId(int id);
        void Add(User user);
    }
}