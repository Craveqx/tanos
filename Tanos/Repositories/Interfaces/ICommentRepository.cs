﻿using Tanos.Models.Dbo;

namespace Tanos.Repositories.Interfaces
{
    public interface ICommentRepository
    {
        void Add(Comment comment);
    }
}