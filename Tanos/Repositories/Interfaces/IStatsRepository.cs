﻿using Tanos.Models.Dbo;

namespace Tanos.Repositories.Interfaces
{
    public interface IStatsRepository
    {
        void AddStat(AppStat appStat);
    }
}