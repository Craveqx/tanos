﻿using Tanos.Models.Dbo;

namespace Tanos.Repositories.Interfaces
{
    public interface IPostRepository
    {
        void Add(Post post);
        Post Get(int id);
        Post GetByOwnerAndId(int postId, int wallOwnerId);
    }
}