﻿namespace Tanos.Models
{
    public enum VkEventType
    {
        Add =0,
        Update = 1,
        Remove =2
    }
}