﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Tanos.Models
{
    public class WallComment: VkEvent{

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("from_id")]
        public int AuthorId { get; set; }

        [JsonProperty("post_id")]
        public int PostId { get; set; }

        [JsonProperty("post_owner_id")]
        public int PostOwner { get; set; }

        [JsonConverter(typeof(UnixDateTimeConverter))]
        [JsonProperty("date")]
        public int Date { get; set; }
    }
}