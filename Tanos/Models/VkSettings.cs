﻿namespace Tanos.Models
{
    public class VkSettings
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}