﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Tanos.Models.Events
{
    public class NewPostEvent : VkEvent
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("owner_id")]
        public int WallOwnerId { get; set; }

        [JsonProperty("from_id")]
        public int AuthorId { get; set; }

        [JsonProperty("text")]
        public  string Text { get; set; }

        [JsonConverter(typeof(UnixDateTimeConverter))]
        [JsonProperty("date")]
        public DateTime Date { get; set; }
    }
}