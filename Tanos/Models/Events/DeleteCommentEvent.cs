﻿using Newtonsoft.Json;

namespace Tanos.Models.Events
{
    public class DeleteCommentEvent : VkEvent
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("owner_id")]
        public int WallOwnerId { get; set; }

        [JsonProperty("deleter_id")]
        public int DeleterId { get; set; }

        [JsonProperty("post_id")]
        public int PostId { get; set; }
       
    }

}