﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Tanos.Models.Events
{
    public class NewCommentEvent : VkEvent
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("from_id")]
        public int AuthorId { get; set; }

        [JsonProperty("post_id")]
        public int PostId { get; set; }

        [JsonProperty("post_owner_id")]
        public int PostOwner { get; set; }

        [JsonConverter(typeof(UnixDateTimeConverter))]
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }
    }
}