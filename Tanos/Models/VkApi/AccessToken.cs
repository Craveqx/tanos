﻿using System;

namespace Tanos.Models.VkApi
{
    public class AccessToken
    {

        public AccessToken(string token, int expiredInSeconds)
        {
            if (expiredInSeconds < 0)
            {
                throw new ArgumentException("Cannot be negative", nameof(expiredInSeconds));
            }

            Token = token ?? throw new ArgumentNullException(nameof(token));
            ExpiredIn = DateTime.Now.AddSeconds(expiredInSeconds);
        }

        public string Token { get; }

        public DateTime ExpiredIn { get; }

        public bool IsExpired => DateTime.Now > ExpiredIn;
    }
}