﻿

using Newtonsoft.Json;

namespace Tanos.Models
{
    public class VkRawEvent
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("object")]
        public object JsonObject { get; set; } 

        [JsonProperty("group_id")]
        public int GroupId { get; set; }
    }
}