﻿namespace Tanos.Models.Dbo
{
    /// <summary>
    ///     Активность пользователя.
    /// </summary>
    public class UserActivity
    {
        /// <summary>
        ///     Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Ид пользователя.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        ///     Ид. группы.
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        ///     Ид. поста.
        /// </summary>
        public int? PostId { get; set; }

        /// <summary>
        ///     Ид. коммента.
        /// </summary>
        public int? CommentId { get; set; }

        /// <summary>
        ///     Тип действия.
        /// </summary>
        public UserActionType ActionType { get; set; }

        /// <summary>
        ///     Пользователь.
        /// </summary>
        public User User { get; set; }

        /// <summary>
        ///     Пост.
        /// </summary>
        public Post Post { get; set; }

        /// <summary>
        ///     Коммент.
        /// </summary>
        public Comment Comment { get; set; }
    }


    public enum UserActionType
    {
        AddComment = 0,
        AddPost = 1,
        DeleteComment = 3
    }
}