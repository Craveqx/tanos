﻿namespace Tanos.Models.Dbo
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User
    {
        /// <summary>
        ///     Идентификатор пользователя в вк.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Полное имя.
        /// </summary>
        public string Name => $"{LastName} {FirstName}";

        /// <summary>
        /// Имя.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Адрес фото.
        /// </summary>
        public string PhotoUrl { get; set; }

        /// <summary>
        /// Идентификатор пользователя вк.
        /// </summary>
        public int VkAuthorId { get; set; }
    }
}