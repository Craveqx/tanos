﻿using System;
using System.Collections.Generic;

namespace Tanos.Models.Dbo
{
    public partial class AppStat
    {
        public int Id { get; set; }
        public DateTimeOffset Created { get; set; }
        public bool DeletePost { get; set; }
        public bool RepostFromGroup { get; set; }
        public int ModerId { get; set; }
        public int? GroupId { get; set; }
        public bool Ban { get; set; }
        public bool Publication { get; set; }
        public bool DeleteComment { get; set; }

        public AppGroup Group { get; set; }
        public AppModer Moder { get; set; }
    }
}
