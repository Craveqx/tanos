﻿using System;

namespace Tanos.Models.Dbo
{
    /// <summary>
    /// Комментарий.
    /// </summary>
    public class Comment
    {
        /// <summary>
        ///     Идентификатор коммента в вк.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Текст коммента.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        ///     Идентификатор автора.
        /// </summary>
        public int AuthorId { get; set; }

        /// <summary>
        ///     Автор.
        /// </summary>
        public User Author { get; set; }

        /// <summary>
        ///     Дата комментария.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Идентификатор комментария.
        /// </summary>
        public int VkCommentId { get; set; }
    }
}