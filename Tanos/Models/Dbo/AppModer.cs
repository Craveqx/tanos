﻿using System;
using System.Collections.Generic;

namespace Tanos.Models.Dbo
{
    public partial class AppModer
    {
        public AppModer()
        {
            AppStat = new HashSet<AppStat>();
        }

        public int Id { get; set; }
        public int VkId { get; set; }
        public string Name { get; set; }

        public ICollection<AppStat> AppStat { get; set; }
    }
}
