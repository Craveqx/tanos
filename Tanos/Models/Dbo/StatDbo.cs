﻿using System;

namespace Tanos.Models.Dbo
{
    public class StatDbo
    {
        public int Id { get; set; }

        public DateTime Created { get; set; }

        public int GroupId { get; set; }

        public int ModerId { get; set; }

        public bool IsDeletePost  { get; set; }

        public bool IsBan { get; set; }

        public bool IsRepostedFromGroup { get; set; }

        public bool IsPublication { get; set; }

        public bool IsDeleteComment { get; set; }
    }
}