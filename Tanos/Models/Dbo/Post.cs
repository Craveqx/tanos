﻿using System;

namespace Tanos.Models.Dbo
{
    public class Post
    {
        /// <summary>
        ///     Идентификатор поста в вк.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///     Текст поста.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        ///     Дата поста.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        ///     Идентификатор автора.
        /// </summary>
        public int AuthorId { get; set; }

        /// <summary>
        ///     Автор.
        /// </summary>
        public User Author { get; set; }

        /// <summary>
        ///     Идентификатор поста в вк.
        /// </summary>
        public int VkPostId { get; set; }
    }
}