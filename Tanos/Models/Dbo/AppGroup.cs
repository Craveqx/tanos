﻿using System;
using System.Collections.Generic;

namespace Tanos.Models.Dbo
{
    public partial class AppGroup
    {
        public AppGroup()
        {
            AppStat = new HashSet<AppStat>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int GroupId { get; set; }

        public ICollection<AppStat> AppStat { get; set; }
    }
}
