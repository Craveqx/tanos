﻿using Microsoft.AspNetCore.Mvc;
using Tanos.Factories;
using Tanos.Logic;
using Tanos.Models;
using Tanos.Repositories;

namespace Tanos.Controllers
{
    [Route("api/[controller]")]
    public class VkCallbackController : Controller
    {
        private readonly IEventRegistrator _eventRegistrator;

        public VkCallbackController(IEventRegistrator eventRegistrator)
        {
            this._eventRegistrator = eventRegistrator;
        }

        [HttpGet("{id}")]
        public IActionResult Registrate(int id)
        {
            return Ok("ok");
        }

        [HttpPost]
        public IActionResult Index([FromBody]VkRawEvent rawEvent)
        {
            _eventRegistrator.AddEvent(VkEventFactory.Create(rawEvent));
            return Ok();
        }
    }
}