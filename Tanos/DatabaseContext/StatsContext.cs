﻿using Microsoft.EntityFrameworkCore;
using Tanos.Models.Dbo;

namespace Tanos.DatabaseContext
{
    public partial class StatsContext : DbContext
    {
        public virtual DbSet<AppGroup> AppGroup { get; set; }
        public virtual DbSet<AppModer> AppModer { get; set; }
        public virtual DbSet<AppStat> AppStat { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //optionsBuilder.UseNpgsql(@"Host=localhost;Database=raenza_vk_bot;Username=postgres;Password=postgres");
                optionsBuilder.UseSqlite("Data Source=stats.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppGroup>(entity =>
            {
                entity.ToTable("app_group");

                entity.HasIndex(e => e.GroupId)
                    .HasName("app_group_group_id_989ea357_uniq")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.Name).HasColumnName("name");
            });

            modelBuilder.Entity<AppModer>(entity =>
            {
                entity.ToTable("app_moder");

                entity.HasIndex(e => e.VkId)
                    .HasName("app_moder_vk_id_82871075_uniq")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.VkId).HasColumnName("vk_id");
            });

            modelBuilder.Entity<AppStat>(entity =>
            {
                entity.ToTable("app_stat");

                entity.HasIndex(e => e.GroupId)
                    .HasName("app_stat_group_id_6e45e74f");

                entity.HasIndex(e => e.ModerId)
                    .HasName("app_stat_moder_id_fddf5586");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Ban).HasColumnName("ban");

                entity.Property(e => e.Created).HasColumnName("created");

                entity.Property(e => e.DeleteComment).HasColumnName("delete_comment");

                entity.Property(e => e.DeletePost).HasColumnName("delete_post");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.ModerId).HasColumnName("moder_id");

                entity.Property(e => e.Publication).HasColumnName("publication");

                entity.Property(e => e.RepostFromGroup).HasColumnName("repost_from_group");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.AppStat)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("app_stat_group_id_6e45e74f_fk_app_group_id");

                entity.HasOne(d => d.Moder)
                    .WithMany(p => p.AppStat)
                    .HasForeignKey(d => d.ModerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("app_stat_moder_id_fddf5586_fk_app_moder_id");
            });
        }
    }
}
