﻿using Microsoft.EntityFrameworkCore;
using Tanos.Models.Dbo;

namespace Tanos.DatabaseContext
{
    public class UserActivityContext: DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserActivity> UsersActivity { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }

        public virtual DbSet<Post> Posts { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                //optionsBuilder.UseNpgsql(@"Host=localhost;Database=raenza_group_activity;Username=postgres;Password=postgres");
                optionsBuilder.UseSqlite("Data Source=userActivity.db");
            }
        }

    }
}