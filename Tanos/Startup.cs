﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Tanos.Logic;
using Tanos.Models;
using Tanos.Providers;
using Tanos.Repositories.Implementations;
using Tanos.Repositories.Interfaces;
using VkApiClient.Providers;
using VkApiClient.WebClient;

namespace Tanos
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<VkSettings>(Configuration.GetSection("VkSettings").Bind);

            services.AddMvc();
            services.AddSingleton<IVkAccessTokenProvider, VkAccessTokenProvider>();
            services.AddSingleton<IVkRestApiClient, VkRestApiClient>();
            services.AddScoped<IVkWebClient, VkWebClient>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<ICommentRepository, CommentRepository>();
            services.AddScoped<IStatsRepository, StatsRepository>();
            services.AddScoped<IUserActivityRepository, UserActivityRepository>();
            services.AddScoped<IActivityConverterFactory, ActivityConverterFactory>();
            services.AddScoped<IEventRegistrator, EventRegistrator>();
            services.AddScoped<IVkObjectsProvider, VkObjectsProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
