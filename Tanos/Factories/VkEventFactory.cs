﻿using System;
using Newtonsoft.Json.Linq;
using Tanos.Models;
using Tanos.Models.Events;

namespace Tanos.Factories
{
    public static class VkEventFactory
    {
        public static VkEvent Create(VkRawEvent rawEvent)
        {
            VkEvent result = null;
            if (rawEvent.JsonObject is JObject jobject)
            {
                if (rawEvent.Type.StartsWith("wall_reply_new"))
                {
                    result = jobject.ToObject<NewCommentEvent>();

                }

                if (rawEvent.Type.StartsWith("wall_post_new"))
                {
                    result = jobject.ToObject<NewPostEvent>();
                }

                if (rawEvent.Type.StartsWith("wall_reply_delete"))
                {
                    result = jobject.ToObject<DeleteCommentEvent>();

                }

                if (result == null)
                {
                    throw new InvalidOperationException(
                        "Json не удалось проебразовать в подходящий объект. Возможно события не поддерживаются");
                }

                result.GroupId = rawEvent.GroupId;
               
            }
            return result;

        }
    }
}