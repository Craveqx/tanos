﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Tanos.Migrations.Stats
{
    public partial class IniatialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "app_group",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    group_id = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "app_moder",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    name = table.Column<string>(nullable: false),
                    vk_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_moder", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "app_stat",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ban = table.Column<bool>(nullable: false),
                    created = table.Column<DateTimeOffset>(nullable: false),
                    delete_comment = table.Column<bool>(nullable: false),
                    delete_post = table.Column<bool>(nullable: false),
                    group_id = table.Column<int>(nullable: true),
                    moder_id = table.Column<int>(nullable: false),
                    publication = table.Column<bool>(nullable: false),
                    repost_from_group = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_app_stat", x => x.id);
                    table.ForeignKey(
                        name: "app_stat_group_id_6e45e74f_fk_app_group_id",
                        column: x => x.group_id,
                        principalTable: "app_group",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "app_stat_moder_id_fddf5586_fk_app_moder_id",
                        column: x => x.moder_id,
                        principalTable: "app_moder",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "app_group_group_id_989ea357_uniq",
                table: "app_group",
                column: "group_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "app_moder_vk_id_82871075_uniq",
                table: "app_moder",
                column: "vk_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "app_stat_group_id_6e45e74f",
                table: "app_stat",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "app_stat_moder_id_fddf5586",
                table: "app_stat",
                column: "moder_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "app_stat");

            migrationBuilder.DropTable(
                name: "app_group");

            migrationBuilder.DropTable(
                name: "app_moder");
        }
    }
}
