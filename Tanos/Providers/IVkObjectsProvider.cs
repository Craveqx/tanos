﻿using Tanos.Models.Dbo;

namespace Tanos.Providers
{
    public interface IVkObjectsProvider
    {
        Post GetPost(int postId, int wallOwnerId);
        User GetUser(int vkUserId);
    }
}