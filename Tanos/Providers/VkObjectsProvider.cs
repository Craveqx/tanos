﻿using System;
using Tanos.Models.Dbo;
using Tanos.Repositories.Interfaces;
using VkApiClient.WebClient;

namespace Tanos.Providers
{
    public class VkObjectsProvider : IVkObjectsProvider
    {
        private readonly IPostRepository _postRepository;
        private readonly IUserRepository _userRepository;
        private readonly IVkWebClient _vkWebClient;

        public VkObjectsProvider(
            IVkWebClient vkWebClient,
            IUserRepository userRepository,
            IPostRepository postRepository)
        {
            _vkWebClient = vkWebClient;
            _userRepository = userRepository;
            _postRepository = postRepository;
        }

        public Post GetPost(int postId, int wallOwnerId)
        {
            var post = _postRepository.GetByOwnerAndId(postId, wallOwnerId);
            if (post == null)
            {
                var vkPost = _vkWebClient.GetPostById($"{wallOwnerId}_{postId}").Result;
                if (vkPost == null)
                    throw new InvalidOperationException($"Пост с идентификатором {wallOwnerId} не найден");
                var author = GetUser(vkPost.AuthorId);
                post = new Post
                {
                    Date = vkPost.Date,
                    Text = vkPost.Text,
                    AuthorId = author.Id,
                    VkPostId = vkPost.Id
                };
                _postRepository.Add(post);
            }

            return post;
        }

        public User GetUser(int vkUserId)
        {
            var user = _userRepository.GetUserByVkId(vkUserId);
            if (user == null)
            {
                var vkUser = _vkWebClient.GetUserById(vkUserId).Result;
                if (vkUser == null)
                    throw new InvalidOperationException($"Пользователь с идентификатором {vkUserId} не найден");
                user = new User
                {
                    FirstName = vkUser.FirstName,
                    LastName = vkUser.LastName,
                    VkAuthorId = vkUser.Id
                };
                _userRepository.Add(user);
            }

            return user;
        }
    }
}