﻿using System;
using System.Net;
using System.Threading.Tasks;
using CSharpFunctionalExtensions;
using Microsoft.Extensions.Options;
using Tanos.Models;
using VkApiClient.Models;
using VkApiClient.Providers;
using VkApiClient.WebClient;

namespace Tanos.Providers
{
    /// <summary>
    /// Провайдер токена доступа для вк.
    /// </summary>
    public class VkAccessTokenProvider: IVkAccessTokenProvider
    {
        private readonly NetworkCredential _credential;

        private static VkAccessToken Token => null;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="credOptions">Учетные данные.</param>
        public VkAccessTokenProvider(IOptions<VkSettings> credOptions)
        {
            var options = credOptions.Value ?? throw new ArgumentNullException(nameof(credOptions));
            _credential = new NetworkCredential(options.Username, options.Password);
        }

        /// <summary>
        /// Возвращает токен.
        /// </summary>
        /// <returns>Объект токена.</returns>
        public async Task<Result<VkAccessToken>> GetToken()
        {
            if (Token == null || Token.IsExpired)
            {
                return await VkAuthHandler.Login(_credential);
            }
            return Result.Ok(Token);
        }
    }
}