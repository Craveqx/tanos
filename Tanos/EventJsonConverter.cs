﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Tanos.Models;
using Tanos.Models.Events;

namespace Tanos
{
    public class EventJsonConverter
    {
        public static VkRawEvent Convert(string json)
        {
            return JsonConvert.DeserializeObject<VkRawEvent>(json);
        }
    }
}