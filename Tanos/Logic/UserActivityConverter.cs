﻿using Tanos.Models;
using Tanos.Models.Dbo;
using Tanos.Providers;
using Tanos.Repositories;
using Tanos.Repositories.Interfaces;

namespace Tanos.Logic
{
    public abstract class UserActivityConverter
    {
        protected IVkObjectsProvider VkObjectsProvider;
        protected IPostRepository PostRepository;
        public abstract UserActivity Convert(VkEvent vkEvent);
    }
}