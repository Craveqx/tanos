﻿using System;
using Tanos.Models;
using Tanos.Models.Dbo;
using Tanos.Models.Events;
using Tanos.Providers;
using Tanos.Repositories.Interfaces;

namespace Tanos.Logic
{
    public class NewPostActivityConverter : UserActivityConverter
    {
        public NewPostActivityConverter(
            IVkObjectsProvider vkObjectsProvider,
            IPostRepository postRepository)
        {
            VkObjectsProvider = vkObjectsProvider;
            PostRepository = postRepository;
        }

        public override UserActivity Convert(VkEvent vkEvent)
        {
            if (!(vkEvent is NewPostEvent postAddEvent))
                throw new ArgumentNullException(nameof(postAddEvent));

            var post = CreatePost(postAddEvent);
            return new UserActivity
            {
                ActionType = UserActionType.AddPost,
                UserId = post.AuthorId,
                PostId = post.Id,
                GroupId = vkEvent.GroupId
            };
        }

        private Post CreatePost(NewPostEvent postAddEvent)
        {
            var author = VkObjectsProvider.GetUser(postAddEvent.AuthorId);
            var post = new Post
            {
                VkPostId = postAddEvent.Id,
                //Author = author,
                Date = postAddEvent.Date,
                AuthorId = author.Id,
                Text = postAddEvent.Text
            };
            PostRepository.Add(post);
            return post;
        }
    }
}