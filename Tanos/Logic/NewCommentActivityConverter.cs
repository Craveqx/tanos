﻿using System;
using Tanos.Models;
using Tanos.Models.Dbo;
using Tanos.Models.Events;
using Tanos.Repositories;
using Tanos.Repositories.Interfaces;

namespace Tanos.Logic
{
    public class NewCommentActivityConverter:UserActivityConverter
    {
        private readonly ICommentRepository _commentRepository;

        public NewCommentActivityConverter(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public override UserActivity Convert(VkEvent vkEvent)
        {
            if (!(vkEvent is NewCommentEvent commentEvent))
                throw new ArgumentNullException(nameof(commentEvent));

            var post = VkObjectsProvider.GetPost(commentEvent.PostId, commentEvent.GroupId);
            var comment = CreateComment(commentEvent);
            return new UserActivity()
            {
                ActionType = UserActionType.AddComment,
                PostId = post.Id,
                UserId = comment.AuthorId,
                CommentId = comment.Id,
                GroupId = vkEvent.GroupId
            };
        }

        private Comment CreateComment(NewCommentEvent commentEvent)
        {
            var author = VkObjectsProvider.GetUser(commentEvent.AuthorId);
            var comment = new Comment()
            {
                AuthorId = author.Id,
                Date = commentEvent.Date,
                Text = commentEvent.Text
            };
            _commentRepository.Add(comment);
            return comment;

        }
    }
}