﻿using System;
using Tanos.Models;
using Tanos.Models.Dbo;
using Tanos.Repositories;
using Tanos.Repositories.Interfaces;

namespace Tanos.Logic
{
    public interface IEventRegistrator
    {
        void AddEvent(VkEvent vkEvent);
    }

    public class EventRegistrator : IEventRegistrator
    {
        private readonly IStatsRepository _statRepository;
        private readonly IUserActivityRepository _userActivityRepository;
        private readonly IActivityConverterFactory _converterFactory;

        public EventRegistrator(IStatsRepository statRepository, 
            IActivityConverterFactory converterFactory, 
            IUserActivityRepository userActivityRepository)
        {
            _statRepository = statRepository;
            _converterFactory = converterFactory;
            _userActivityRepository = userActivityRepository;
        }


        public void AddEvent(VkEvent vkEvent)
        {
            try
            {
                DoAddEvent(vkEvent);
            }
            catch (Exception e)
            {
                var mesasge = $"Error add event. {e}";
                Console.WriteLine(mesasge);
                throw new InvalidOperationException("Failed to register event");
            }
            
        }

        private void DoAddEvent(VkEvent vkEvent)
        {
            var activityConverter = _converterFactory.Create(vkEvent);
            var userActivity = activityConverter.Convert(vkEvent);
            _userActivityRepository.Add(userActivity);
            AddStat(vkEvent);
        }

        private void AddStat(VkEvent vkEvent)
        {
            var appStat = AppStatFactory.Create(vkEvent);
            if (appStat != null)
            {
                _statRepository.AddStat(appStat);
            }
        }
    }
}