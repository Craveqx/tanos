﻿using System;
using Tanos.Models;
using Tanos.Models.Dbo;
using Tanos.Models.Events;

namespace Tanos.Logic
{
    public static class AppStatFactory
    {
        public static AppStat Create(VkEvent vkEvent)
        {
            switch (vkEvent)
            {
                case  DeleteCommentEvent e:
                    return new AppStat()
                    {
                        DeleteComment = true,
                        Created = new DateTimeOffset(DateTime.Now),
                        GroupId = e.GroupId,
                        ModerId = e.DeleterId,
                    };
                default:
                    return null;
            }
        }
    }
}