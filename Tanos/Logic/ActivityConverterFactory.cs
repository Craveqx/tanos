﻿using System;
using Tanos.Models;
using Tanos.Models.Events;
using Tanos.Providers;
using Tanos.Repositories;
using Tanos.Repositories.Interfaces;

namespace Tanos.Logic
{
    public interface IActivityConverterFactory
    {
        UserActivityConverter Create(VkEvent vkEvent);
    }

    public class ActivityConverterFactory : IActivityConverterFactory
    {
        private readonly IVkObjectsProvider vkObjectsProvider;
        private readonly IPostRepository postRepository;
        private ICommentRepository commentRepository;

        public ActivityConverterFactory(IPostRepository postRepository, 
            IVkObjectsProvider vkObjectsProvider, 
            ICommentRepository commentRepository)
        {
            this.postRepository = postRepository;
            this.vkObjectsProvider = vkObjectsProvider;
            this.commentRepository = commentRepository;
        }

        public UserActivityConverter Create(VkEvent vkEvent)
        {
            switch (vkEvent)
            {
                case NewPostEvent e:
                    return new NewPostActivityConverter(vkObjectsProvider, postRepository);
                case NewCommentEvent e:
                    return new NewCommentActivityConverter(commentRepository);
                default:
                    throw new InvalidOperationException("Не поддерживаемый тип события");
            }
        }
    }
}