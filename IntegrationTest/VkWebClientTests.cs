using System;
using Microsoft.Extensions.Options;
using Tanos.Models;
using Tanos.Providers;
using VkApiClient.WebClient;
using Xunit;

namespace IntegrationTest
{
    /// <summary>
    /// ����� ��� ������� ��.
    /// </summary>
    public class VkWebClientTests
    {
        private readonly VkRestApiClient _vkRestApiClient;

        /// <summary>
        /// �����������.
        /// </summary>
        public VkWebClientTests()
        {
            var options = Options.Create(new VkSettings()
            {
                Username = "hellqx@mail.ru",
                Password = "dingo9050331bingo"
            });
             var vkAccessTokenProvider = new VkAccessTokenProvider(options);
             _vkRestApiClient = new VkRestApiClient(vkAccessTokenProvider);

        }


        /// <summary>
        /// ���� ������ �� ��������� ������������. ��������, ���� ��� ������� ���������.
        /// </summary>
        [Fact]
        public void GetUserById_NameEqualsToExpected()
        {
            VkWebClient  vkWebClient  = new VkWebClient(_vkRestApiClient);
            var result = vkWebClient.GetUserById(512937).Result;

            Assert.Equal("������� �����", $"{result.LastName} {result.FirstName}");
        }

        /// <summary>
        /// ���� ������ �� ��������� �����. ��������, ���� ����� ����� �� ������.
        /// </summary>
        [Fact]
        public void GetPostById_TextNotEmpty()
        {
            VkWebClient vkWebClient = new VkWebClient(_vkRestApiClient);
            var result = vkWebClient.GetPostById("-29411916_44845").Result;

            Assert.NotEmpty(result.Text);
        }


    }
}
